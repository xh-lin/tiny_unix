# asm startup file
# very first module in load

# hw2.doc:
#   3) The Finished Program:
#       Startup0 executes first, and transfers control to the kernel initialization in tunix.c,
#       which sets up the system and starts the user code at ustart (calling ustart will do the trick).

# Lecture09[1].ppt, page 13:
#   exit code:
#       Add a label to int $3 to make it callable
#       For suggested step 3: pass a bogus exit code
#       For suggested step 4: pass the exit code from main

.text
.globl _start, _startupc, _return2Tutor

_start:   
    movl $0x3ffff0, %esp   # set user program stack
    movl $0, %ebp          # make debugger backtrace terminate here
    call _startupc         # call C startup, which calls user main

_return2Tutor:
    int $3                 # use $3 to call a trap service handler to return to Tutor


