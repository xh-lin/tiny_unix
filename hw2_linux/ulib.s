# file :	ulib.s
# description:	assembler routines for read, write, exit
# hw2:	 add read, exit				

# following Linux system call linkage
# user stack when execution reaches _write:	
# esp-> return addr
#       first arg  (to be put in ebx for syscall)
#       second arg (to be put in ecx)
#       third arg  (to be put in edx)

# user stack after pushl %ebx, needed to preserve %ebx (not a C scratch reg)
# esp->  saved-ebx
# 4(esp) return addr
# 8(esp) first arg  (to be put in ebx for syscall)
# 12(esp) second arg (to be put in ecx)
# 16(esp) third arg  (to be put in edx)

# =============================================================================
# hw2.doc
#	1. Introduction:
#		Prototypes:
#			int read(int dev, char *buf, int nchar);  /* read nchar bytes into buf from dev */ 
#			int write(int dev, char *buf, int nchar);  /* write nchar bytes from buf to dev */ 
#			int exit(int exitcode);    /* exit will return back to Tutor */ 
#	2. Discussions:
#		To make system calls, the user program needs to execute a trap instruction to transfer 
#		control to the kernel and the kernel’s trap handler will perform the service. 
#		The x86 Linux syscall linkage is as follows: 
#			--int 0x80 is the syscall instruction 
#			--the syscall # is in eax 
#			--the syscall args are in ebx (first), ecx (second), and edx (third).
# 	iii) User-level files: 
# 	ulib.s:
#		library set-ups for syscalls: _read, _write, _exit. 
#		Provided for write, you add read and exit. 
# 	3) The Finished Program:
# 		... The syscalls in the user code (in ulib.s, called from
# 		test1.c) cause execution of the system call handler in tunix.c 
#		(and functions called from there), returning to the user code 
#		in ulib.s at the iret. ...

# Lecture09[1].ppt, page 5, 6, :
#	System Call Steps in hw2:
#		2. Calls in user library
#		3. ulib copies dev, buf,nbytes off the stack into registers, put 3 in
#		   eax as syscall number. Trap to kernel by executing int $0x80
#		4. CPU trap cycle handler: save CPU stack on stack, get new  		
#		   EIP=(address of _syscall) from trap vector IDT[0x80]
#		9. Finish lib which ends with “ret” which restores saved 
#		   EIP and the “ret” causes function return
#	User Program Flow,
#	6)lib calls to do write, read, exit:
#		ulib.s: syscall lib setups; call trap handler; _write provided
#		-add _read
#		-add _exit

# Suggested Steps:
# 	2. Make test1.c do a simple write. ulib.s is set up for write already, 
#	so do the write syscall first. ...
# =============================================================================

# syscall #s (tsyscall.h):
# SYS_EXIT    1
# SYS_READ    3
# SYS_WRITE   4

.text
.globl _write, _read, _exit	

_exit:
	pushl %ebx            		# save the value of ebx
	movl 8(%esp),%ebx             	# first arg in ebx		<- int dev
	movl $1,%eax                  	# syscall # in eax		<- SYS_EXIT
        int $0x80                   	# trap to kernel
	popl  %ebx             		# restore the value of ebx
	ret

_read:
	pushl %ebx            		# save the value of ebx
	movl 8(%esp),%ebx             	# first arg in ebx		<- int dev
	movl 12(%esp),%ecx            	# second arg in ecx		<- char *buf
	movl 16(%esp),%edx            	# third arg in edx		<- int nchar
	movl $3,%eax                  	# syscall # in eax		<- SYS_READ
        int $0x80                   	# trap to kernel
	popl  %ebx             		# restore the value of ebx
	ret

_write:	
	pushl %ebx            		# save the value of ebx
	movl 8(%esp),%ebx             	# first arg in ebx		<- int dev
	movl 12(%esp),%ecx            	# second arg in ecx		<- char *buf
	movl 16(%esp),%edx            	# third arg in edx		<- int nchar
	movl $4,%eax                  	# syscall # in eax		<- SYS_WRITE
        int $0x80                   	# trap to kernel
	popl  %ebx             		# restore the value of ebx
	ret







