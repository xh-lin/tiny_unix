/*
    hw2.doc:
        i) Shared between user and kernel: 
        tsyscall.h:
            syscall numbers (like the UNIX /usr/include/sys/syscall.h) 
*/

// Linux System Call Table:
// http://shell-storm.org/shellcode/files/syscalls.html
#define SYS_EXIT    1
#define SYS_READ    3
#define SYS_WRITE   4

