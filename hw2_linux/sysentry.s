# hw2.doc:
#   ii) Kernel files: 
#   sysentry.s:
#       Trap handler's assembler envelope routine _syscall for the trap 
#       resulting from a system call--needs to push eax, ebx, ecx, edx 
#       on stack, where they can be accessed from C, call _syscallc, 
#       then pops, iret. Use $pclibsrc/irq4.s as an example and modify 
#       it to fit your needs.

# Lecture09[1].ppt, page 5, 7:
#   System Call Steps in hw2:
#       5. _syscall executes, saves registers on the stack and
#          call C syscall handler (_syscallc) 
#       8. Return to _syscall, restore registers and do final iret
#   Kernel Program Flows,
#   1)Trap handler wrapper:
#       sysentry.s: Modify from  $pclibsrc/irq4.s
#       -push eax,ebx,ecd,edx on stack
#       -call system call dispatcher: _syscallc
#       -pop stack and iret

# Suggested Steps:
#   -have it push registers on the stack and then call into tunix.c.


.text
.globl syscallc, _syscall

# Lecture09[1].ppt, page 11, syscall in sysentry.s:
_syscall:
    pushl %edx      # push the values of eax to edx to stack
    pushl %ecx
    pushl %ebx
    pushl %eax
    call syscallc  # call C trap routine in tunix.c
    popl %eax       # or replace it with add $4, %esp to  
                    # skip over the eax from the stack
    popl %ebx       # pop the values of ebx to edx from stack
    popl %ecx
    popl %edx
    iret
