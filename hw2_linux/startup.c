/* C startup file, called from startup0.s-- */

extern void clr_bss(void);
extern void init_devio(void);
//extern void main(void);
extern void Init_kernel(void);  // my kernel initialization routine

// prototype:
void _startupc(void);

/*
  hw2.doc:
    ii) Kernel files:
    startup.c:
      you need to modify it to call your kernel initialization routine instead of main.

  Lecture09[1].ppt, page 6:
    User Program Flow,
    Init memory:
      startup.c: 
        Modify from $pclibsrc/startup.c to call init kernel instead of main

  Piazza @65:
    In startup.c, instead of calling main(), call an init function in tunix.c 
    which does ioinit() (which initialize interrupts, traps), then it calls main(). 
    When it returns to startup.c  from main(), do a shutdown()
*/
void _startupc()
{
  clr_bss();			    /* clear BSS area (uninitialized data) */
  init_devio();			  /* latch onto Tutor-supplied info, code */
  //(void)main();			/* execute user-supplied main */
  Init_kernel();      // call init kernel instead of main

}
