/*
    hw2.doc:
        ii) Kernel files: 
        tunix.c:
            has kernel init routine. It needs to call ioinit, 
            call set_trap_gate(0x80,&syscall), and possibly other inits. 
            tunix.c also has the code for syscallc, sys call exit, and set_trap_gate. 
            The code for set_trap_gate is just that of set_intr_gate with the line: 
                    desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_INTGATE;
            Replaced by
                    desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_TRAPGATE; 
        
            You can find the locate_idt function in $pclibsrc/cpureg.S. 
            Make sure you include the following prototype if you want to 
            call it using C:
                    extern void locate_idt(unsigned int *limitp, char ** idtp); 
    
        3) The Finished Program:
            ...Startup0 executes first, and transfers control to the kernel initialization in tunix.c,
            which sets up the system and starts the user code at ustart (calling ustart will do the trick).
            ...The syscalls in the user code (in ulib.s, called from test1.c) cause execution of the system 
            call handler in tunix.c (and functions called from there), returning to the user code in ulib.s 
            at the iret.

    Lecture09[1].ppt, page 5, 6:
        System Call Steps in hw2:
            6. It accesses the saved registers via its args, determines
               from the syscall number that it should call sysread
            7. sysread executes, does the input and return to _syscallc
        User Program Flow,
        3)Init kernel:
            Part of tunix.c: new module
            -call ioinit
            -call set_trap_gate(0x80, &syscall) modified from set_intr_gate in $pclibsrc/cpu.c
            -call _ustart()
        Kernel Program Flows,
        2)System call dispatcher:
            Part of tunix.c: 
            -depend on syscall #, call the handler routine

    Suggested Steps: 
        1. ... write a tiny tunix.c init function that calls ioinit,
           then calls main (cheating for now--later it should call ustart),
           and then returns to startup, shutting down. ...
        2. ... Set the trap vector up in kernel init in tunix.c, and write sysentry.s—have it push registers 
           on the stack and then call into tunix.c.  In tunix.c, access the pushed registers (themselves syscall 
           arguments from the user) via args to the C function, and call syswrite.
*/

#include <stdio.h>
#include <cpu.h>
#include <gates.h>
#include "tsyscall.h"       // syscall #s:  SYS_EXIT 1,  SYS_READ 3,  SYS_WRITE 4
#include "tsystm.h"         // ioinit(), sysread(), syswrite(), syscontrol(), debug_log()   

extern IntHandler _syscall;
extern void _ustart(void);           // in crt0.s: ini stack, call main, push %eax, call exit
extern void _return2Tutor(void);     // in startup0.s: int $3   # return to Tutor
extern void locate_idt(unsigned int *limitp, char ** idtp); // for set_trap_gate()

// prototypes in this file:
void Init_kernel(void);
void syscallc(int user_eax, int arg1, char * arg2, int arg3);
int syscall_exit(int exitcode);
void set_trap_gate(int n, IntHandler *inthand_addr);


/* kernel init routine */
void Init_kernel()
{
    /*
        Piazza @64:
        Any time you need to program registers in hardware, you need to do it within the cli() and sti() block.
    */
    cli();                              // [[ disable interrupt
    ioinit();                               // initialize interrupts, traps (Piazza @65)
    set_trap_gate(0x80, &_syscall);         // in this file: void set_trap_gate(int n, IntHandler *inthand_addr)
    sti();                              // ]] re-enable interrupt
    _ustart();                          // in crt0.s
}


/*
    hw2.doc:
        syscallc:
            first write it with a big switch statement over the various different syscalls. If you have time,
            upgrade it to use a sysent dispatch table. 
        sysentry.s:
            ... call _syscallc, ...

    Lecture09[1].ppt, page 5, 7, 12:
        System Call Steps in hw2,
            sysentry.s:
                5. _syscall executes, saves registers on the stack and call C syscall handler (_syscallc) 
            io.c,tunix.c:
                7. sysread executes, does the input and return to _syscallc
        Kernel Program Flows,
        1)Trap handler wrapper:
            sysentry.s: ... -call system call dispatcher: _syscallc
        The syscallc C program:
            Using the stack-register-stack method, we can call a C function using an assembler routine

void syscallc(int syscall#, int dev,  char * buf,  int nchar)*/
void syscallc(int user_eax, int arg1, char * arg2, int arg3)
{
    switch(user_eax)    // get syscall#
    {
        case SYS_EXIT:  // if syscal# = 1
            //     int syscall_exit(int exitcode)               // in this file
            user_eax = syscall_exit(arg1);                      // put func return value into %eax
            break;
        case SYS_READ:  // if syscal# = 3
            //     int sysread(int dev, char *buf, int nchar)   // in tsystm.h, io.c
            user_eax = sysread(arg1, arg2, arg3);                // put func return value into %eax
            break;
        case SYS_WRITE: // if syscal# = 4
            //     int syswrite(int dev, char *buf, int nchar)  // in tsystm.h, io.c
            user_eax = syswrite(arg1, arg2, arg3);              // put func return value into %eax
            break;
        default:
            kprintf("\n funny syscall#: %d \n", user_eax);
    }
}


/*
    syscall exit:
        Suggested Steps:
            3. Next implement syscall exit and add an exit to test1.c.
            4. ... it does an exit syscall. ... Try a user program without its own exit syscall.
*/
int syscall_exit(int exitcode)
{
    kprintf("\n syscall_exit(), exitcode from user program: %d \n", exitcode);
    _return2Tutor();     // in startup0.s
    return 0;           // in syscallc(): need to put return value into user_eax
}


/*
    hw2.doc:
        The code for set_trap_gate is just that of set_intr_gate with the line: 
                desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_INTGATE;
        Replaced by
                desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_TRAPGATE; 
*/
void set_trap_gate(int n, IntHandler *inthand_addr)
{
    //debug_set_intr_gate(n, inthand_addr, 0);
    int debug = 0;

    char *idt_LAaddr;		/* linear address of IDT */
    char *idt_VAaddr;		/* virtual address of IDT */
    Gate_descriptor *idt, *desc;
    unsigned int limit = 0;

    if (debug)
        printf("Calling locate_idt to do sidt instruction...\n");
    locate_idt(&limit,&idt_LAaddr);
    /* convert to virtual address, i.e., ordinary address */
    idt_VAaddr = idt_LAaddr - KERNEL_BASE_LA;  /* usable address of IDT */
    /* convert to a typed pointer, for an array of Gate_descriptor */
    idt = (Gate_descriptor *)idt_VAaddr;
    if (debug)
        printf("Found idt at %x, lim %x\n",idt, limit);
    desc = &idt[n];		/* select nth descriptor in idt table */
    /* fill in descriptor */
    if (debug)
        printf("Filling in desc at %x with addr %x\n",(unsigned int)desc,
        (unsigned int)inthand_addr);
    desc->selector = KERNEL_CS;	/* CS seg selector for int. handler */
    desc->addr_hi = ((unsigned int)inthand_addr)>>16; /* CS seg offset of inthand  */
    desc->addr_lo = ((unsigned int)inthand_addr)&0xffff;
    //desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_INTGATE; /* valid, interrupt */
    desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_TRAPGATE;   // my modification for hw2: set_trap_gate()
    desc->zero = 0;
}
