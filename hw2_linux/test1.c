/*
    hw2.doc:
        iii) User-level files: 
        test1.c: 
            has main().  Easily extended to multiple user files, or user assembler 
            sources, as long as they follow the syscall rules and have a _main 
            entry point. First example is 
                    main() { write(TTY1,"hi!\n",4);}.
            Work back to testio.c from hw1.
        crt0.s:
            ... calls _main, ...
        3) The Finished Program:
            ... The C user startup module reinitializes the stack and calls main. ...

    Lecture09[1].ppt, page 6, 13:
        User Program Flow,
        4)Start-up:
            crt0.s: ... -call _main
        5)User program:
            uprog.c: Modify from testio.c:
            -have a main
            -remove ioinit
            -call write
            -call read
        exit code:
            startup0.s: ... call _startupc ... # call C startup, which calls user main
            * For suggested step 4: pass the exit code from main

    Suggested Steps:
        1. ...At this point, test1.c just has a main that kprintf's a message.
           Now it should build and run, but does no syscalls.
        2. ...Make test1.c do a simple write.
        3. Next implement syscall exit and add an exit to test1.c.
        4. ... Change the call to main in kernel initialization to call ustart now. 
           ... Modify the final test1.c to perform the same operations as in testio.c 
           and create a typescript file that captures its execution. 
*/

#include <stdio.h>
#include "tunistd.h"    // has "tty_public.h", exit(), read(), write(), control()

#define MILLION 1000000
#define DELAYLOOPCOUNT (400 * MILLION)
#define BUFLEN 80

void delay(void);


int main(void)
{
    char buf[BUFLEN];
    int got, i, lib_console_dev, ldev;
    
    /* Determine the SAPC's "console" device, the serial port for user i/o */
    lib_console_dev = sys_get_console_dev();  /* SAPC support lib fn */
    switch(lib_console_dev)
    {
        case COM1: 
            ldev = TTY0;	/* convert to our dev #'s */
            break;
        case COM2: 
            ldev = TTY1;
            break;
        default: 
            printf("Unknown console device\n");
            return 1;   // exitcode: Exit Fail
    }
    kprintf("Running with device TTY%d\n",ldev);
    /* Now have a usable device to talk to with i/o package-- */

    //ioinit();  /* Initialize devices */
    kprintf("\nTrying simple write(4 chars)...\n");
    got = _write(ldev,"hi!\n",4);
    kprintf("write of 4 returned %d\n",got);
    delay();  /* time for output to finish, once write-behind is working */

    kprintf("Trying longer write (9 chars)\n");
    got = _write(ldev, "abcdefghi", 9);
    kprintf("write of 9 returned %d\n",got);
    delay();  /* time for output to finish, once write-behind is working */

    for (i = 0; i < BUFLEN; i++)
        buf[i] = 'A'+ i/2;
    kprintf("\nTrying write of %d-char string...\n", BUFLEN);
    got = _write(ldev, buf, BUFLEN);
    kprintf("\nwrite returned %d\n", got);
    delay();

    kprintf("\nType 10 chars input to test typeahead while looping for delay...\n");
    delay();
    got = _read(ldev, buf, 10);	/* should wait for all 10 chars, once fixed */
    kprintf("\nGot %d chars into buf. Trying write of buf...\n", got);
    _write(ldev, buf, got);

    kprintf("\nTrying another 10 chars read right away...\n");
    got = _read(ldev, buf, 10);	/* should wait for input, once fixed */
    kprintf("\nGot %d chars on second read\n",got);
    if (got == 0) 
        kprintf("nothing in buffer\n");	/* expected result until fixed */
    else 
        _write(ldev, buf, got);	/* should write 10 chars once fixed */

    // don't need to implement control() in hw2
    /* 
    kprintf("\n\nNow turning echo off--\n");
    control(ldev, ECHOCONTROL, 0);
    kprintf("\nType 20 chars input, note lack of echoes...\n");
    delay();
    got = read(ldev, buf, 20);
    kprintf("\nTrying write of buf...\n");
    write(ldev, buf, got);
    kprintf("\nAsked for 20 characters; got %d\n", got);
    */

    return 0;   // exitcode: Exit Successful
}

/* the faster the machine you're on, the longer this loop must be */
void delay()
{
    int i;
    kprintf("<doing delay>\n");
    for (i = 0; i < DELAYLOOPCOUNT; i++)
        ;
}