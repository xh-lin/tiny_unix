# hw2.doc:
#   iii) User-level files: 
#   crt0.s:
#       user-level "C startup module"  sets up stack, 
#       calls _main, does exit syscall.  Entry point _ustart.

# Lecture09[1].ppt, page 6:
#   User Program Flow,
#   4)Start-up:
#       crt0.s: Modify from $pclibsrc/startup0.s
#       -init user stack
#       -call  _main
#       -call _exit

# Suggested Steps:
#   Write the proper user startup module crt0.s to reinitialize 
#   the stack and call into main, then when that returns, 
#   it does an exit syscall.

.text
.globl _ustart, main, _exit

_ustart:   
    movl $0x3ffff0, %esp    # set user program stack
    movl $0, %ebp           # make debugger backtrace terminate here
    call main               # main() in user program: test1.c
    pushl %eax              # push exit code and give to syscall_exit()
    call _exit              # in ulib.s:    _exit

