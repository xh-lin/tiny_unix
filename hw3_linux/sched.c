#include <stdio.h>
#include <cpu.h>
#include "sched.h"

//extern void asmswtch(PEntry oldpentryp, PEntry newpentryp);
extern void asmswtch(PEntry * oldpentryp, PEntry * newpentryp); // need to pass by ref
extern void debug_log(char *);

int schedule(void)
{
    // 1. Logic to determine which process to run
    int saved_eflags, i, nextProcIdx = 0, zombieCount = 0, oldProcIdx;
    char log[20];

    saved_eflags = get_eflags();
    cli();
    
    for(i = 1; i < 4; i++) {
        if (proctab[i].p_status == ZOMBIE)
            zombieCount++;
        if (proctab[i].p_status == RUN) {
            nextProcIdx = i;
            break;
        }
    }
    // 2. Provide oldproc, curproc; asmswtch(oldpentryp, newpentryp)
    oldProcIdx = currentProcIdx;
    currentProcIdx = nextProcIdx;
    // debug log
    sprintf(log,"|(%c%c-%c)", '0' + oldProcIdx, 
                              proctab[oldProcIdx].p_status == ZOMBIE ? 'z' : ' ', 
                              '0' + currentProcIdx);
    debug_log(log);
    //asmswtch(proctab[oldProcIdx], proctab[currentProcIdx]);   // wrong
    asmswtch(proctab + oldProcIdx, proctab + currentProcIdx);   // need to pass pointer

    set_eflags(saved_eflags);

    // 3. After return, it is running the new process
    if (zombieCount == 3)
        return allZombies;
    return notAllZombies;
}
/*
    Block the calling process by setting the process status = BLOCKED and waitcode = event.
    Run the scheduler to start another process.
    Disable interrupt at the beginning and restore interrupt at the end.
    Called from ttywrite (to replace the busy wait).
    Modify ttywrite in hw2 to include sleep():
        while (enqueue(…) = = FULLQUE)
            sleep(OUTPUT);
*/
void sleep(WaitCode event)
{
    int saved_eflags;
    saved_eflags = get_eflags();
    cli();

    proctab[currentProcIdx].p_status = BLOCKED;
    proctab[currentProcIdx].p_waitcode = event;
    schedule();

    set_eflags(saved_eflags);
}

/*
    Go through all user processes. If process status = BLOCKED and waitcode = event, 
        then change process status = RUN.
    Disable interrupt at the beginning and restore interrupt at the end.
    Called from tty output interrupt handler.
    For wakeup(), it is only called from the interrupt handler with IF =0. 
        Make sure the wakeup code does not set IF=1.
    wakeup() does not call schedule(). It just set p_status = RUN.
*/
void wakeup(WaitCode event)
{
    int saved_eflags, i;
    saved_eflags = get_eflags();
    cli();

    for (i = 1; i < 4; i++) {
        if(proctab[i].p_status == BLOCKED && proctab[i].p_waitcode == event)
            proctab[i].p_status = RUN;
    }

    set_eflags(saved_eflags);
}

