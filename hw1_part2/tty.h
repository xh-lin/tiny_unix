/*********************************************************************
*
*       file:           tty.h
*       author:         betty o'neil
*
*       private header file for tty drivers
*       apps should not include this header
*/

#ifndef TTY_H
#define TTY_H

#define MAXBUF 6

#include "queue/queue.h"  // my

struct tty {
  int echoflag;			/* echo chars in read */
  //char rbuf[MAXBUF];            /* typeahead chars */
  //int rin;                    /* index into receive buffer */
  //int rout;                   /* index out of receive buffer */
  //int rnum;                   /* number of characters in rcvr buffer */
  //char tbuf[MAXBUF];            /* output chars (for transmit) */
  //int tin;                    /* index into transmit buffer */
  //int tout;                   /* index out of transmit buffer */
  //int tnum;                   /* number of characters in xmit buffer */

  // my replacement queues for rbuf[] and tbuf[]:
  Queue rbufq;  // my read buffer queue, readqueue, RX_Queue
  Queue tbufq;  // my transmit buffer queue, writequeue, TX_Queue
  Queue ebufq;  // my echo buffer queue, Echo_queue
};

extern struct tty ttytab[];

/* tty-specific device functions */
void ttyinit(int dev);
int ttyread(int dev, char *buf, int nchar);
int ttywrite(int dev, char *buf, int nchar);
int ttycontrol(int dev, int fncode, int val);

#endif 
