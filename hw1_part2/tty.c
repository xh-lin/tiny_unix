/*********************************************************************
*
*       file:           tty.c
*       author:         betty o'neil
*
*       Modification:
*       CS444   hw1_part2   Xu Huang Lin    3/10/2020
*       a.  repalce ring buffers with queues
*       b.  makes ttyread to wait for inputs
*       c.  makes ttywrite interrupt driven
*
*       tty driver--device-specific routines for ttys 
*
*/
#include <stdio.h>  /* for kprintf prototype */
#include <serial.h>
#include <cpu.h>
#include <pic.h>
#include "ioconf.h"
#include "tty_public.h"
#include "tty.h"

//extern int kprintf(char * fmt, ...);
/* tell C about the assembler shell routines */
extern void irq3inthand(void), irq4inthand(void);

/* C part of interrupt handlers--specific names called by the assembler code */
extern void irq3inthandc(void), irq4inthandc(void); 

/* the common code for the two interrupt handlers */                          
static void irqinthandc(int dev); 
struct tty ttytab[NTTYS];        /* software params/data for each SLU dev */

// my
/* Record debug info in otherwise free memory between program and stack */
/* 0x300000 = 3M, the start of the last M of user memory on the SAPC */
#define DEBUG_AREA 0x300000
#define BUFLEN 20
char *debug_log_area = (char *)DEBUG_AREA;
char *debug_record;  /* current pointer into log area */ 
/* prototype for debug_log */ 
void debug_log(char *);

/*====================================================================
*
*       tty specific initialization routine for COM devices
*
*/

void ttyinit(int dev)
{
  int baseport;
  struct tty *tty;		/* ptr to tty software params/data block */

  debug_record = debug_log_area; /* clear debug log */  // my
  baseport = devtab[dev].dvbaseport; /* pick up hardware addr */
  tty = (struct tty *)devtab[dev].dvdata; /* and software params struct */

  if (baseport == COM1_BASE) {
      /* arm interrupts by installing int vec */
      set_intr_gate(COM1_IRQ+IRQ_TO_INT_N_SHIFT, &irq4inthand);
      pic_enable_irq(COM1_IRQ);
  } else if (baseport == COM2_BASE) {
      /* arm interrupts by installing int vec */
      set_intr_gate(COM2_IRQ+IRQ_TO_INT_N_SHIFT, &irq3inthand);
      pic_enable_irq(COM2_IRQ);
  } else {
      kprintf("Bad TTY device table entry, dev %d\n", dev);
      return;			/* give up */
  }
  tty->echoflag = 1;		/* default to echoing */
  //tty->rin = 0;               /* initialize indices */
  //tty->rout = 0;
  //tty->rnum = 0;              /* initialize counter */
  //tty->tin = 0;               /* initialize indices */
  //tty->tout = 0;
  //tty->tnum = 0;              /* initialize counter */

  // my Part_A:  replace ring buffers with queues
  // my initializing queues:
  init_queue(&(tty->rbufq), MAXBUF);  // my read buffer queue, readqueue, RX_Queue
  init_queue(&(tty->tbufq), MAXBUF);  // my transmit buffer queue, writequeue, TX_Queue
  init_queue(&(tty->ebufq), MAXBUF);  // my echo buffer queue, Echo_queue

  /* enable interrupts on receiver */
  outpt(baseport+UART_IER, UART_IER_RDI); /* RDI = receiver data int */
}


/*====================================================================
*
*       Useful function when emptying/filling the read/write buffers
*
*/

#define min(x,y) (x < y ? x : y)


/*====================================================================
*
*       tty-specific read routine for TTY devices
*
*/

// my Part_B: Fix only take type-ahead to make it wait for all inputs
int ttyread(int dev, char *buf, int nchar)
{
  //int baseport;
  struct tty *tty;
  //int i, copychars;
  int i;  // my
  int saved_eflags;  // my
  char log[BUFLEN];  // my for debug log

  // baseport = devtab[dev].dvbaseport; /* hardware addr from devtab */
  tty = (struct tty *)devtab[dev].dvdata;   /* software data for line */

  /*
  //copychars = min(nchar, tty->rnum);      // chars to copy from buffer
  copychars = min(nchar, queuecount(&(tty->rbufq)));  // my queue replacing
  for (i = 0; i < copychars; i++) {
    saved_eflags = get_eflags();
    cli();			// disable ints in CPU
    //buf[i] = tty->rbuf[tty->rout++];      // copy from ibuf to user buf
    //tty->rnum--;                          // decrement count
    //if (tty->rout >= MAXBUF) tty->rout = 0;
    buf[i] = dequeue(&(tty->rbufq));  // my queue replacing
    set_eflags(saved_eflags);     // back to previous CPU int. status
  }
  */

// my fix to wait for all inputs:
i = 0;
while (i < nchar) { // my loop forever until nchar number of characters have been read
  saved_eflags = get_eflags();
  cli();
  if (emptyqueue(&(tty->rbufq)) == TRUE)
    ; // my wait for readqueue buffer to fill up
  else {
    buf[i++] = dequeue(&(tty->rbufq));  // my if isn't empty, read a char
    // my for debug log
    sprintf(log, ">%c", buf[i]);
    debug_log(log);
  }
  set_eflags(saved_eflags);
}

  return i; // my number of copied chars (should be samme as nchar)
  //return copychars;       /* but should wait for rest of nchar chars if nec. */
  /* this is something for you to correct */
}


/*====================================================================
*
*       tty-specific write routine for SAPC devices
*       (cs444: note that the buffer tbuf is superfluous in this code, but
*        it still gives you a hint as to what needs to be done for
*        the interrupt-driven case)
*
*/

int ttywrite(int dev, char *buf, int nchar)
{
  //kprintf("  [[enter ttywrite]]\n"); // my for debug
  //int baseport;
  struct tty *tty;
  int i;
  int saved_eflags; // my
  int baseport = devtab[dev].dvbaseport;  // my
  char log[BUFLEN];  // my for debug log

  //baseport = devtab[dev].dvbaseport; /* hardware addr from devtab */
  tty = (struct tty *)devtab[dev].dvdata;   /* software data for line */
  
  /*
  for (i = 0; i < nchar; i++) {
    //tty->tbuf[tty->tin++] = buf[i];
    //tty->tnum++;
    //if (tty->tin >= MAXBUF) tty->tin = 0;
    saved_eflags = get_eflags();  // my
    cli();  // my
    enqueue(&(tty->tbufq), buf[i]);  // my queue replacing
    set_eflags(saved_eflags);  // my
    putc(dev+1, buf[i]);	// use lib for now--replace this!
  }
  */

  // my 1. Load the first part of the string into the output buffer 
  // my (i.e. enqueue the chars until the queue is full (Qmax)).
  saved_eflags = get_eflags();
  cli();
  i = 0;
  while((i < nchar) && (enqueue(&(tty->tbufq), buf[i]) != FULLQUE)) {
    // my for debug log
    sprintf(log,"<%c", buf[i]); /* record input char-- */
    debug_log(log);
    i++;
  }
  set_eflags(saved_eflags);

  //kprintf("    [[done loading first part]]\n"); // my for debug
  // my 2. Turn Tx interrupt on in the UART_IER.
  // my I use bitwise OR becasue I just want to turn on THRI only
  outpt(baseport+UART_IER, inpt(baseport+UART_IER) | UART_IER_THRI);

  //kprintf("    [[start loop and load rest]]\n"); // my for debug
  // my 3. While the interrupt is ongoing, 
  // my loop over the enqueuing of the rest of the chars.
  while (i < nchar) { // my while there's more chars need to read
    saved_eflags = get_eflags();
    cli();
    while (enqueue(&(tty->tbufq), buf[i]) == FULLQUE)
      ; // my wait for TX int to dequeue
    set_eflags(saved_eflags);

    // my for debug log
    sprintf(log,"<%c", buf[i]); /* record input char-- */
    debug_log(log);
    i++; // my now enqueued a char

    //kprintf("      [[enQ a char successful]]\n"); // my for debug
    // my turn on TX interrupt just in case
    // my b/c TX int will be turned off if trans int outputting too fast
    outpt(baseport+UART_IER, inpt(baseport+UART_IER) | UART_IER_THRI);
  }

  //kprintf("  [[exit ttywrite]]\n"); // my for debug
  return i; // my # of enqueued chars (should be same as nchar if successful)
  //return nchar;
}

/*====================================================================
*
*       tty-specific control routine for TTY devices
*
*/

int ttycontrol(int dev, int fncode, int val)
{
  struct tty *this_tty = (struct tty *)(devtab[dev].dvdata);

  if (fncode == ECHOCONTROL)
    this_tty->echoflag = val;
  else return -1;
  return 0;
}



/*====================================================================
*
*       tty-specific interrupt routine for COM ports
*
*   Since interrupt handlers don't have parameters, we have two different
*   handlers.  However, all the common code has been placed in a helper 
*   function.
*/
  
void irq4inthandc()
{
  irqinthandc(TTY0);
}                              
  
void irq3inthandc()
{
  irqinthandc(TTY1);
}                              

void irqinthandc(int dev){  
  int ch;
  struct tty *tty = (struct tty *)(devtab[dev].dvdata);
  int baseport = devtab[dev].dvbaseport; /* hardware i/o port */
  int iir;  // my

  pic_end_int();                /* notify PIC that its part is done */

  iir = inpt(baseport + UART_IIR);  // my
  switch (iir & UART_IIR_ID) {
    case UART_IIR_RDI:  /* it's a receiver int */
      //kprintf("  <<enter RX int>>\n"); // my for debug
      debug_log("*"); // my for debug log
      ch = inpt(baseport+UART_RX);	/* read char, ack the device */
      //if (tty->rnum < MAXBUF) {   /* if space left in ring buffer */
      if (queuecount(&(tty->rbufq)) < MAXBUF) { // my
        //tty->rnum++;                 /* increase character count */
        //tty->rbuf[tty->rin++] = ch; /* put char in ibuf, step ptr */
        //if (tty->rin >= MAXBUF)     /* check if we need to wrap-around */
        //  tty->rin = 0;              /* and reset as appropriate */
        enqueue(&(tty->rbufq), ch); // my
      }
      if (tty->echoflag) {             /* if echoing wanted */
        //outpt(baseport+UART_TX,ch);   /* echo char: see note above */
        enqueue(&(tty->ebufq), ch);  // my store input char in echo buffer
        outpt(baseport+UART_IER, inpt(baseport+UART_IER) | UART_IER_THRI);  // my turn on TX int
      }
      //kprintf("  <<exit RX int>>\n"); // my for debug
    break;

    // my Part_C: make ttywrite from polls to interrupt driven
    case UART_IIR_THRI: /*  it is a transmitter int */
      //kprintf("  <<enter TX int>>\n"); // my for debug
      debug_log("^"); // my for debug log
      // my output preferentially from the echo queue
      if (emptyqueue(&(tty->ebufq)) == FALSE)
        outpt(baseport+UART_TX, dequeue(&(tty->ebufq)));
      // my 1. Check if the queue is empty.
      else if (emptyqueue(&(tty->tbufq)) == FALSE)
        // my 2. If there are chars, dequeue one and output it to UART_TX.
        outpt(baseport+UART_TX, dequeue(&(tty->tbufq)));
      else  // my if both tx queue and echo queue are empty:
        // my 3. If there none, shut down Tx interrupt in the UART_IER.
        // my bitwise AND NOT so turn off TX int only
        outpt(baseport+UART_IER, inpt(baseport+UART_IER) & ~UART_IER_THRI);
      //kprintf("  <<exit TX int>>\n"); // my for debug
    break;

    default:
      kprintf("irqinthandc(): default case error, IIR_ID: %x", iir & UART_IIR_ID);
  }
}

/* append msg to memory log */
void debug_log(char *msg)
{
    strcpy(debug_record, msg);
    debug_record +=strlen(msg);
}
